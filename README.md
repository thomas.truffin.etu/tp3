<img src="images/readme/header.jpg">

## Objectifs
- Savoir manipuler la page HTML avec l'API DOM
- Savoir détecter les actions de l'utilisateur avec les Event
- Être capable de gérer des formulaires avec JS
- Utiliser la History API

## Sommaire
Pour plus de clarté, les instructions du TP se trouvent dans des fichiers distincts (un fichier par sujet), procédez dans l'ordre sinon, ça fonctionnera beaucoup moins bien !

1. [A. Préparatifs](A-preparatifs.md)
2. [B. Les bases de l'API DOM](B-les-bases.md)
3. [C. Les événements](C-evenements.md)
4. [D. Les formulaires](D-formulaires.md)
5. [E. Navigation avancée](./E-navigation-avancee.md)