import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";
console.log(document.querySelectorAll(' article h4')[1].innerHTML);
const element = document.querySelectorAll('header a')[0];

element.setAttribute('class', element.getAttribute('class') + ' active');
document.querySelector('.newsContainer').setAttribute('style', '');

Router.menuElement = document.querySelector('.mainMenu');

document
	.querySelector('.closeButton')
	.addEventListener('click', removeNewsContainer);

function removeNewsContainer(event) {
	event.preventDefault();
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
	console.log(document.querySelector('.newsContainer').getAttribute('style'));
}
